syntax on
set nocompatible              " be iMproved, required by vundle
filetype off                  " required by vundle

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

"mostly vundel stuff from here
call vundle#begin()

" let Vundle manage Vundle, required by vundle
Plugin 'gmarik/Vundle.vim'

" The following are the plugins I'm using
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo(i.e add www.github.com/*whatever is inside plugin ' ' to visit the site

Plugin 'scrooloose/nerdtree'
Plugin 'bling/vim-airline'

" All of your Plugins must be added before the following line


call vundle#end()            " required by vundle
filetype plugin indent on    " required by bundle
" To ignore plugin indent changes, instead use:
" filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" vundle stuff ends here

"airline stuff
"airline always appears
set laststatus=2
let g:airline_powerline_fonts = 1

"general configurations
"use j k t move one step up down not newline
nmap j gj
nmap k gk
"set to auto read when a file is changed from the outside
set autoread

set wildmenu            " visual autocomplete for command menu

"it is nice to have an indication of the line number right there in the editor
set number

set t_Co=256 "set terminal colors

set incsearch           " search as characters are entered
set hlsearch            " highlight matches

